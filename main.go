package main

import (
	"fmt"
	"log"
	"math"
	"math/cmplx"
	"os"
	"strconv"
)

const (
	N = 128
	numberIteration = 7
	bitsNumber = 7
	filenameRes = "results.txt"
)

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func originalFunction(x float64) complex128 {
	return complex(x * x, 0)
}

func reverseBits(number int) int {

	var (
		invNumber int
		i         int
	)

	invNumber = 0
	for i = 0; i < bitsNumber; i++ {
		invNumber <<= 1
		invNumber |= number & 1
		number >>= 1
	}

	return invNumber

}

func makeTransform(N int, T float64) ([]float64, []float64) {

	var (
		realA     []float64
		imagA     []float64
		a         []complex128
		b         []complex128
		i, i1, i2 int
		i3, i4    int
		revIndex  int
	)

	/* create arrays */
	a = make([]complex128, N)
	b = make([]complex128, N)
	imagA = make([]float64, N)
	realA = make([]float64, N)

	/* fill arrays using initial values */
	i = 0
	for i < N {
		a[i] = complex(0.0, 0.0)
		i++
	}
	i = 0
	for i < N {
		/* use inverse order of bits */
		revIndex = reverseBits(i)
		b[revIndex] = originalFunction(float64(i) * T / float64(N))
		i++
	}

	/* do FFT */
	i = 0
	i1 = 1
	/* go through basic iterations */
	for i < numberIteration {
		/* go through "small" element */
		i2 = 0
		for i2 < N {
			/* do calculation in one "small" element */
			i3 = 0
			for i3 < i1 {
				a[i3 + i2] = b[i3 + i2] + cmplx.Exp(complex(0.0,-math.Pi * float64(i3) / float64(i1))) * b[i3 + i2 + i1]
				a[i3 + i2 + i1] = b[i3 + i2] - cmplx.Exp(complex(0.0,-math.Pi * float64(i3) / float64(i1))) * b[i3 + i2 + i1]
				i3++
			}
			/* go to next "small" element */
			i2 += 2 * i1
		}
		/* increase size of "small" element */
		i1 = i1 * 2
		/* save new iteration in additional array */
		i4 = 0
		for i4 < N {
			b[i4] = a[i4]
			i4++
		}
		i++
	}

	/* save results in two float64 arrays */
	i = 0
	for i < N {
		realA[i] = real(a[i])
		imagA[i] = imag(a[i])
		i++
	}

	/* give results */
	return realA, imagA

}

func writeResults(realA []float64, imagA []float64, N int, T float64) {

	var (
		fileRes *os.File
		err     error
		i       int
	)

	fileRes, err = os.Create(filenameRes)
	handleError(err)


	_, err = fileRes.WriteString("# T = " + strconv.FormatFloat(T, 'f', -1, 64) + " N = " +
									strconv.Itoa(N) + "\n")
	handleError(err)

	_, err = fileRes.WriteString("# imagA realA\n")
	handleError(err)

	for i < N {
		_, err = fileRes.WriteString(strconv.FormatFloat(realA[i], 'f', -1, 64) + " " +
			strconv.FormatFloat(imagA[i], 'f', -1, 64) + "\n")
		handleError(err)
		i++
	}

}

func readFloatNumber(msg string, number * float64) {

	var (
		err error
	)

	fmt.Println(msg)
	_, err = fmt.Scanln(number)
	handleError(err)

}

func main() {

	var (
		T      float64
		realA  []float64
		imagA  []float64
	)

	readFloatNumber("Period of function:", &T)

	realA, imagA = makeTransform(N, T)
	writeResults(realA, imagA, N, T)

}